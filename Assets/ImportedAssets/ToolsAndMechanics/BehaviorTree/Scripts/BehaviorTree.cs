using UnityEngine;

namespace ToolsAndMechanics.BehaviorTree
{
    public abstract class BehaviorTree : MonoBehaviour
    {
        protected Node _root = null;

        protected virtual void Start()
        {
            _root = SetupTree();
        }

        protected virtual void Update()
        {
            if (_root != null)
                _root.Evaluate();
        }

        protected abstract Node SetupTree();
    }
}