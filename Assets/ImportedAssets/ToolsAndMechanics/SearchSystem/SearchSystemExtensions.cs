using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ToolsAndMechanics.SearchSystem
{
    using Utilities;

    public static class SearchSystemExtensions
    {
        public static T GetNearestObject<T>(this Vector3 pos, List<T> list, float min = 0, float max = float.MaxValue, bool checkPriority = false, bool ignoreY = true) where T : IFindable
        {
            IFindable nearest = null;
            float minDistance = max;
            int maxPriority = int.MinValue;

            foreach (var obj in list)
            {
                float distance = ignoreY ? MathExtensions.GetDistanceXZ(pos, obj.Position) :
                    Vector3.Distance(pos, obj.Position);

                if (checkPriority && maxPriority > obj.Priority)
                    continue;

                if (distance > minDistance || distance < min)
                    continue;

                minDistance = distance;
                nearest = obj;
                maxPriority = nearest.Priority;
            }
            return (T)nearest;
        }

        public static T GetNearestObject<T>(this T main, List<T> list, bool includeSelf = true, float min = 0, float max = float.MaxValue, bool checkPriority = false, bool ignoreY = true) where T : IFindable
        {
            if (!includeSelf)
            {
                var tmp = new List<T>(list);
                tmp.Remove(main);
                return GetNearestObject(main.Position, tmp, min, max, checkPriority, ignoreY);
            }
            return GetNearestObject(main.Position, list, min, max, checkPriority, ignoreY);
        }

        public static List<T> GetObjectsAtRange<T>(this Vector3 pos, List<T> list, float min = 0, float max = float.MaxValue, bool ignoreY = true) where T : IFindable
        {
            if (ignoreY)
            {
                return list.Where(c => !MathExtensions.IsDistanceLessXZ(pos, c.Position, min) &&
                               MathExtensions.IsDistanceLessXZ(pos, c.Position, max)).ToList();
            }
            else
            {
                return list.Where(c => !MathExtensions.IsDistanceLess(pos, c.Position, min) &&
                               MathExtensions.IsDistanceLess(pos, c.Position, max)).ToList();
            }
        }

        public static List<T> GetObjectsAtRange<T>(this T main, List<T> list, bool includeSelf = true, float min = 0, float max = float.MaxValue, bool ignoreY = true) where T : IFindable
        {
            if (!includeSelf)
            {
                var tmp = GetObjectsAtRange(main.Position, list, min, max, ignoreY);
                tmp.Remove(main);
                return tmp;
            }
            return GetObjectsAtRange(main.Position, list, min, max, ignoreY);
        }
    }
}