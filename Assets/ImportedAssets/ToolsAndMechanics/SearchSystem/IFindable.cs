﻿using System;
using UnityEngine;

namespace ToolsAndMechanics.SearchSystem
{
    public interface IFindable
    {
        public event Action<IFindable> onRemoved;

        public Vector3 Position { get; set; }
        public int Priority { get; }

    }
}