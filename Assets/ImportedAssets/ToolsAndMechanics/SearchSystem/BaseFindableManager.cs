using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ToolsAndMechanics.SearchSystem
{
    public class BaseFindableManager
    {
        public event Action<IFindable> onObjectAdded = delegate { };
        public event Action<IFindable> onObjectRemoved = delegate { };

        public List<IFindable> Objects { get; protected set; } = new List<IFindable>();

        public virtual void AddObject(IFindable findable)
        {
            if (Objects.Contains(findable)) return;

            Objects.Add(findable);
            onObjectAdded(findable);

            findable.onRemoved += OnObjectRemoved;
        }

        public virtual void RemoveObject(IFindable findable)
        {
            findable.onRemoved -= OnObjectRemoved;

            Objects.Remove(findable);
            onObjectRemoved(findable);
        }

        private void OnObjectRemoved(IFindable obj)
        {
            OnObjectRemoved(obj);
        }
    }
}