using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ToolsAndMechanics.SearchSystem
{
    [RequireComponent(typeof(IFindable))]
    public abstract class BaseAddingManager : MonoBehaviour
    {
        private enum AddingType
        {
            None = 0,
            Awake = 1,
            OnEnable = 2
        }

        [SerializeField] private AddingType _addingType = AddingType.OnEnable;

        protected abstract BaseFindableManager _manager { get; }

        protected IFindable _unit;

        protected virtual void Awake()
        {
            _unit = GetComponent<IFindable>();

            if (_addingType == AddingType.Awake)
                Add();
        }

        private void OnEnable()
        {
            if (_addingType == AddingType.OnEnable)
                Add();
        }

        public void Add()
        {
            _manager.AddObject(_unit);
        }
    }
}