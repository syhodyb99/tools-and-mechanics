using System.Collections;
using System.Collections.Generic;
using ToolsAndMechanics.ObjectPool;
using UnityEngine;
using Zenject;

namespace ToolsAndMechanics.HealthSystem
{
    public abstract class AbstractDamageView : MonoBehaviour
    {
        [SerializeField] protected HealthComponent _healthComponent;

        [Inject] protected IObjectPool _pool;

        protected virtual void OnEnable()
        {
            _healthComponent.onDamaged += OnDamaged;
            _healthComponent.onHealed += OnHealed;
            _healthComponent.onHealthChanged += OnHealthChange;
            _healthComponent.onDied += OnDied;
        }

        protected virtual void OnDisable()
        {
            _healthComponent.onDamaged -= OnDamaged;
            _healthComponent.onHealed -= OnHealed;
            _healthComponent.onHealthChanged -= OnHealthChange;
            _healthComponent.onDied -= OnDied;
        }

        protected virtual void OnDamaged(float value) { }
        protected virtual void OnHealed(float value) { }
        protected virtual void OnHealthChange(float value) { }
        protected virtual void OnDied() { }

    }
}