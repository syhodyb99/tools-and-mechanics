﻿using ToolsAndMechanics.Tweens;
using UnityEngine;

namespace ToolsAndMechanics.HealthSystem
{
    public class ChangeHealthTweenView : AbstractDamageView
    {
        [SerializeField] protected AbstractTween _healTween;
        [SerializeField] protected AbstractTween _damageTween;
        [SerializeField] protected AbstractTween _deathTween;

        protected override void OnDamaged(float value)
        {
            if (_damageTween != null) _damageTween.Execute();
        }

        protected override void OnHealed(float value)
        {
            if (_healTween != null) _healTween.Execute();
        }

        protected override void OnDied()
        {
            if (_deathTween != null) _deathTween.Execute();
        }
    }
}