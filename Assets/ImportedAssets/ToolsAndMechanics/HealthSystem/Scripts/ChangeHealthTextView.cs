﻿using ToolsAndMechanics.ObjectPool;
using ToolsAndMechanics.Utilities;
using UnityEngine;

namespace ToolsAndMechanics.HealthSystem
{
    public class ChangeHealthTextView : AbstractDamageView
    {
        [SerializeField] protected PoolableObjectData _textData;
        [SerializeField] protected Transform _spawnPoint;

        protected override void OnHealthChange(float value)
        {
            Vector3 pos = _spawnPoint.position;
            var text = _pool.GetObject(_textData).GetComponent<TextEffect>();
            text.SetPosition(pos);
            string str = value > 0 ? $"+{value}" : value.ToString();
            text.Spawn(str);
        }
    }
}