using System.Collections;
using System.Collections.Generic;
using ToolsAndMechanics.ObjectPool;
using UnityEngine;

namespace ToolsAndMechanics.HealthSystem
{
    public class ChangeHealthVFXView : AbstractDamageView
    {
        [SerializeField] protected PoolableObjectData _healParticle;
        [SerializeField] protected PoolableObjectData _damageParticle;
        [SerializeField] protected PoolableObjectData _deathParticle;
        [SerializeField] protected Transform _spawnPoint;

        protected override void OnDamaged(float value)
        {
            if (_damageParticle != null) _pool.GetObject(_damageParticle, _spawnPoint.position, true);
        }

        protected override void OnHealed(float value)
        {
            if (_healParticle != null) _pool.GetObject(_healParticle, _spawnPoint.position, true);
        }

        protected override void OnDied()
        {
            if (_deathParticle != null) _pool.GetObject(_deathParticle, _spawnPoint.position, true);
        }
    }
}