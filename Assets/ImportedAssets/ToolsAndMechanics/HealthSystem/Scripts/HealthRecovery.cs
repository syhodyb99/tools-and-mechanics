using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ToolsAndMechanics.HealthSystem
{
    [RequireComponent(typeof(HealthComponent))]
    public class HealthRecovery : MonoBehaviour
    {
        [SerializeField] private float _delayBeforeRecovery = 5f;
        [SerializeField] private float _delayBetweenTicks = 0.1f;
        [SerializeField, Range(0, 100)] private float _healPercent = 1;

        private HealthComponent _healthComponent;

        private Coroutine _waitCoroutine;
        private Coroutine _healCoroutine;

        private void Awake()
        {
            _healthComponent = GetComponent<HealthComponent>();
        }

        private void OnEnable()
        {
            _healthComponent.onDamaged += OnDamaged;
        }

        private void OnDisable()
        {
            _healthComponent.onDamaged -= OnDamaged;
        }

        private void OnDamaged(float health)
        {
            if (_waitCoroutine != null)
            {
                StopCoroutine(_waitCoroutine);
                _waitCoroutine = null;
            }
            if (_healCoroutine != null)
            {
                StopCoroutine(_healCoroutine);
                _healCoroutine = null;
            }

            _waitCoroutine = StartCoroutine(WaitAndRecovery());
        }

        private IEnumerator WaitAndRecovery()
        {
            yield return new WaitForSeconds(_delayBeforeRecovery);
            _healCoroutine = StartCoroutine(RecoveryCoroutine());
            _waitCoroutine = null;
        }

        private IEnumerator RecoveryCoroutine()
        {
            yield return null;

            float heal = _healthComponent.MaxHealth * _healPercent / 100;

            while (_healthComponent.Health < _healthComponent.MaxHealth)
            {
                _healthComponent.ApplyHeal(heal);
                yield return new WaitForSeconds(_delayBetweenTicks);
            }

            _healCoroutine = null;
        }
    }
}