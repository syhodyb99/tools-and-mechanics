using System;
using System.Collections;
using System.Collections.Generic;
using ToolsAndMechanics.UserInterfaceManager;
using UnityEngine;

namespace ToolsAndMechanics.HealthSystem
{
    public class HealthTextView : AbstractText
    {
        [SerializeField] private HealthComponent _healthComponent;

        private void Start()
        {
            OnHealthChange(_healthComponent.MaxHealth);
            _healthComponent.onHealthChanged += OnHealthChange;
        }

        private void OnDestroy()
        {
            _healthComponent.onHealthChanged -= OnHealthChange;
        }

        private void OnHealthChange(float health)
        {
            _text.text = ((int)_healthComponent.Health).ToString();
        }
    }
}