using System;
using System.Collections;
using System.Collections.Generic;
using ToolsAndMechanics.WorldUI;
using UnityEngine;

namespace ToolsAndMechanics.HealthSystem.View
{
    public class HealthView : BaseWorldUIView
    {
        public event Action onViewChanged = delegate { };

        public float AnimatedPercent { get; private set; }
        public float Percent => _healthComponent.Health / _healthComponent.MaxHealth;

        [SerializeField] protected GameObject _activeObject;
        [SerializeField] protected float _speed = 10;
        [SerializeField] protected float _startAnimDelay = 0.2f;
        [SerializeField] protected float _endAnimDelay = 0.2f;
        [SerializeField, Range(0f, 1f)] protected float _enablePercent = 0.2f;

        protected virtual HealthComponent _healthComponent => (Target as HealthTarget).HealthComponent;
        protected Coroutine _healthCoroutine;

        protected bool _prevStateIsDead;

        protected override void Init()
        {
            base.Init();

            _healthComponent.onHealthChanged += OnHealthChange;
            _healthComponent.onDied += OnDied;

            _activeObject.SetActive(Percent <= _enablePercent);
            onViewChanged();
        }

        protected virtual void OnDestroy()
        {
            _healthComponent.onHealthChanged -= OnHealthChange;
            _healthComponent.onDied -= OnDied;
        }

        protected virtual void OnEnable()
        {
            AnimatedPercent = 1;
            onViewChanged();
        }

        protected virtual void OnDisable()
        {
            _healthCoroutine = null;
        }

        public override bool IsEnabled()
        {
            return base.IsEnabled() && !(Target as HealthTarget).HealthComponent.IsDead;
        }

        public float GetValueFromPercent(float percent)
        {
            return percent * _healthComponent.MaxHealth;
        }

        protected virtual void OnDied()
        {
            _prevStateIsDead = true;
        }

        protected virtual void OnHealthChange(float value)
        {
            if (_prevStateIsDead)
            {
                _prevStateIsDead = false;
                AnimatedPercent = 1;
                onViewChanged();
                return;
            }

            _activeObject.SetActive(Percent <= _enablePercent);

            if (_healthCoroutine != null) StopCoroutine(_healthCoroutine);
            if (gameObject.activeInHierarchy) _healthCoroutine = StartCoroutine(HealthAnim());
        }

        protected virtual IEnumerator HealthAnim()
        {
            _activeObject.SetActive(true);
            yield return new WaitForSeconds(_startAnimDelay);

            float currHealth = AnimatedPercent;

            while (currHealth > Percent)
            {
                currHealth -= Time.deltaTime * _speed;
                AnimatedPercent = currHealth;
                onViewChanged();
                yield return null;
            }
            AnimatedPercent = Percent;
            onViewChanged();

            yield return new WaitForSeconds(_endAnimDelay);
            _activeObject.SetActive(Percent <= _enablePercent);
            _healthCoroutine = null;
        }
    }
}