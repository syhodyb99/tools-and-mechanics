﻿using UnityEngine;
using UnityEngine.UI;

namespace ToolsAndMechanics.HealthSystem.View
{
    public class AnimatedImageHealthView : ImageHealthView
    {
        [SerializeField] private Image _animatedImage;

        protected override void OnViewChanged()
        {
            _image.fillAmount = _healthView.Percent;
            _animatedImage.fillAmount = _healthView.AnimatedPercent;
        }
    }
}