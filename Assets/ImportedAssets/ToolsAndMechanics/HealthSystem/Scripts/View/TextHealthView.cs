﻿using TMPro;
using UnityEngine;

namespace ToolsAndMechanics.HealthSystem.View
{
    public class TextHealthView : HealthViewListener
    {
        [SerializeField] private TMP_Text _text;
        [SerializeField] private bool _usePercentView;

        protected override void OnViewChanged()
        {
            if (_usePercentView)
            {
                _text.text = $"{(int)(_healthView.Percent * 100)}%";
            }
            else
            {
                _text.text = $"{(int)(_healthView.GetValueFromPercent(_healthView.Percent))}%";
            }
        }
    }
}