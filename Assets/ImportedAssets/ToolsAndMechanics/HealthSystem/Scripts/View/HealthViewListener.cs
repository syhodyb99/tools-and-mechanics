using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ToolsAndMechanics.HealthSystem.View
{
    [RequireComponent(typeof(HealthView))]
    public abstract class HealthViewListener : MonoBehaviour
    {
        protected HealthView _healthView;

        protected virtual void Awake()
        {
            _healthView = GetComponent<HealthView>();
        }

        protected virtual void OnEnable()
        {
            if (_healthView.Target) OnViewChanged();
            _healthView.onViewChanged += OnViewChanged;
        }

        protected virtual void OnDisable()
        {
            _healthView.onViewChanged -= OnViewChanged;
        }

        protected abstract void OnViewChanged();
    }
}