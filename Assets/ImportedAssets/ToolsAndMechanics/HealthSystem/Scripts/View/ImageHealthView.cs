﻿using UnityEngine;
using UnityEngine.UI;

namespace ToolsAndMechanics.HealthSystem.View
{
    public class ImageHealthView : HealthViewListener
    {
        [SerializeField] protected Image _image;

        protected override void OnViewChanged()
        {
            _image.fillAmount = _healthView.Percent;
        }
    }
}