using System;
using System.Collections;
using System.Collections.Generic;
using ToolsAndMechanics.Utilities;
using UnityEngine;

namespace ToolsAndMechanics.HealthSystem
{
    public class HealthComponent : MonoBehaviour
    {
        public event Action<float> onHealed = delegate { };
        public event Action<float> onDamaged = delegate { };
        public event Action<float> onHealthChanged = delegate { };
        public event Action onDied = delegate { };

        public bool CanBeDamaged { get; private set; } = true;
        public float MaxHealth => _health;
        public bool IsDead => _isDead;
        public float Health
        {
            get
            {
                return _currentHealth;
            }
            protected set
            {
                _currentHealth = Math.Clamp(value, 0, MaxHealth);
            }
        }

        [SerializeField] protected bool _needResetOnAwake = true;
        [SerializeField] protected float _health = 15f;
        [SerializeField, ReadOnly] protected float _currentHealth = 15f;

        protected bool _isDead = false;

        protected virtual void Awake()
        {
            if (_needResetOnAwake)
                ResetHealth();
        }

        public virtual void ResetHealth(float h = -1f)
        {
            _isDead = false;
            if (h != -1) _health = h;
            Health = _health;
            onHealthChanged(Health);
        }

        public virtual void SetLiveWithoutEvent(bool isAlive)
        {
            _isDead = !isAlive;
        }

        public virtual void SetMaxHealth(float health, bool resetToMax = true)
        {
            float scale = 1f;

            if (!resetToMax)
            {
                scale = Math.Min(1f, Health / MaxHealth);
            }

            _health = health;
            Health = MaxHealth * scale;
            onHealthChanged(Health);
        }

        public virtual void SetCanBeDamaged(bool can)
        {
            CanBeDamaged = can;
        }

        public virtual void ApplyDamage(float damage)
        {
            if (_isDead) return;
            if (!CanBeDamaged) return;

            Health -= damage;
            onDamaged(damage);
            onHealthChanged(-damage);

            if (Health <= 0)
            {
                Died();
            }
        }

        public virtual void ApplyHeal(float heal)
        {
            if (_isDead) return;

            Health += heal;
            onHealed(heal);
            onHealthChanged(heal);
        }

        protected virtual void Died()
        {
            _isDead = true;
            onDied();
        }
    }
}