using System.Collections;
using System.Collections.Generic;
using ToolsAndMechanics.WorldUI;
using UnityEngine;

namespace ToolsAndMechanics.HealthSystem
{
    public class HealthTarget : BaseWorldUITarget
    {
        [field: SerializeField] public HealthComponent HealthComponent { get; private set; }
    }
}