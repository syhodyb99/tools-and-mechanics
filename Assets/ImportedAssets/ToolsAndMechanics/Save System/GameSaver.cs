using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ToolsAndMechanics.SaveSystem
{
    public static class GameSaver
    {
        public static SaveData Save
        {
            get
            {
                if (_save == null)
                {
                    LoadSaves();
                }
                return _save;
            }
        }

        private static SaveData _save;

        private const string SAVE_KEY = "ToolsAndMechanics.Utilities.Saves";

        public static void SaveChanged()
        {
            if (_save == null) return;

            string json = JsonUtility.ToJson(Save);
            PlayerPrefs.SetString(SAVE_KEY, json);
            PlayerPrefs.Save();
        }

        private static void LoadSaves()
        {
            string json = PlayerPrefs.GetString(SAVE_KEY, string.Empty);

            if (json == string.Empty)
            {
                _save = new SaveData();
            }
            else
            {
                _save = JsonUtility.FromJson<SaveData>(json);
            }
        }

#if UNITY_EDITOR
        [UnityEditor.MenuItem("Tools/Game Saver/Delete All Saves")]
#endif
        private static void DeleteAllSaves()
        {
            PlayerPrefs.DeleteKey(SAVE_KEY);
            PlayerPrefs.Save();
            Debug.Log("Saves was deleted");
        }

#if UNITY_EDITOR
        [UnityEditor.MenuItem("Tools/Game Saver/Show All Saves")]
#endif
        private static void ShowAllSaves()
        {
            Debug.Log(PlayerPrefs.GetString(SAVE_KEY, string.Empty));
        }
    }
}