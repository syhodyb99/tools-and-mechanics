using UnityEngine;

namespace ToolsAndMechanics.Formula
{
    [CreateAssetMenu(menuName = "ToolsAndMechanics/Formula/Int")]
    public class FormulaIntSO : FormulaSO<int> { }
}