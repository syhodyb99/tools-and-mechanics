﻿using UnityEngine;

namespace ToolsAndMechanics.Formula
{
    [CreateAssetMenu(menuName = "ToolsAndMechanics/Formula/Float")]
    public class FormulaFloatSO : FormulaSO<float> { }
}