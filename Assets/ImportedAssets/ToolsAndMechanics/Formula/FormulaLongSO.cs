﻿using UnityEngine;

namespace ToolsAndMechanics.Formula
{
    [CreateAssetMenu(menuName = "ToolsAndMechanics/Formula/Long")]
    public class FormulaLongSO : FormulaSO<long> { }
}