﻿using UnityEngine;

namespace ToolsAndMechanics.Formula
{
    [CreateAssetMenu(menuName = "ToolsAndMechanics/Formula/Double")]
    public class FormulaDoubleSO : FormulaSO<double> { }
}