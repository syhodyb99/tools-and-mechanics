using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ToolsAndMechanics.ObjectPool
{
    public class PoolableObjectInfo : MonoBehaviour
    {
        public PoolableObjectData Data;
    }
}