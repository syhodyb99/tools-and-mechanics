using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace ToolsAndMechanics.UnitsSystem
{
    using HealthSystem;
    using SearchSystem;

    public class BaseUnit : MonoBehaviour, IFindable
    {
        public event Action<BaseUnit> onDied = delegate { };
        public event Action<IFindable> onRemoved = delegate { };

        public virtual Vector3 Position { get => transform.position; set => transform.position = value; }
        public virtual int Priority => 0;

        public bool IsDead => HealthComponent.IsDead;

        public Vector3 ProjectilePosition => Position + _offsetForProjectileTarget;
        [field: SerializeField] public HealthComponent HealthComponent { get; protected set; }
        [field: SerializeField] public Vector3 _offsetForProjectileTarget { get; protected set; }
        [field: SerializeField] public float ModelSize { get; protected set; } = 1f;

        protected virtual void Awake()
        {
            HealthComponent.onDied += OnDied;
            HealthComponent.onDamaged += OnDamaged;
            HealthComponent.onHealed += OnHealed;
        }

        protected virtual void OnDestroy()
        {
            HealthComponent.onDied -= OnDied;
            HealthComponent.onDamaged -= OnDamaged;
            HealthComponent.onHealed -= OnHealed;
        }

        public Vector3 GetTargetPosition(BaseUnit target)
        {
            Vector3 direction = (Position - target.Position).normalized;
            return target.Position + (target.ModelSize + ModelSize) * direction;
        }

        protected virtual void OnDied()
        {
            onDied(this);
            onRemoved(this);
        }

        protected virtual void OnDamaged(float damage)
        {
        }

        protected virtual void OnHealed(float heal)
        {
        }

        protected virtual void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawWireSphere(Position, ModelSize);
        }
    }
}